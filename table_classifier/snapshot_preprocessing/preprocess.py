import os
import json
import random
import shutil
import numpy as np
import pandas as pd
import multiprocessing
import table_classifier
from pathlib import Path
from typing import List, Union
from innolytiq import Snapshot
from innolytiq.document.field import Field
from innolytiq.document.tag import TableTag
from table_classifier.utils import set_seeds
from cdom.parsers.value_parser import ValueParser


class SnapshotCleaner:
    def __init__(self, snapshot: Snapshot, snapshot_path: Path, finished_doc_ids: set, processes=os.cpu_count() // 2):
        self.snapshot = snapshot
        self.processes = processes
        self.snapshot_path = snapshot_path
        self.finished_doc_ids = finished_doc_ids

    @staticmethod
    def remove_document(path):
        shutil.rmtree(path)
        print(f'removed {path}')

    def remove_if_not_finished_or_no_table_class_tagged(self):
        doc_ids = list(sorted(self.snapshot.documents.keys()))
        with multiprocessing.Pool(processes=self.processes) as p:
            p.map(self._remove_document_if_not_finished_or_no_tags, doc_ids)

    def _remove_document_if_not_finished_or_no_tags(self, doc_id):
        try:
            document = self.snapshot.documents[doc_id]
            not_finished_or_no_tag = (doc_id not in self.finished_doc_ids) or (not document.y['table_class'][0].tags)
            if not_finished_or_no_tag:
                self.remove_document(self.snapshot_path.joinpath(doc_id))
        except Exception as e:
            print(f'error reading doc {doc_id} {str(e)}')
            self.remove_document(self.snapshot_path.joinpath(doc_id))

    @staticmethod
    def _replace_tag_value_from_json(document_json_path: Path):
        try:
            with open(document_json_path, 'r') as f:
                document_json = json.load(f)
        except Exception as e:
            print(f'Failed to read json file {document_json_path} {str(e)}')
            return

        for table_class in document_json['output_fields']['table_class']:
            try:
                table_class['tags'][0]['value'] = table_class['value']
            except IndexError:
                print(f'at least one tag is empty in {document_json_path}')

        with open(document_json_path, 'w') as f:
            json.dump(document_json, f)

    def replace_tag_value_from_documents(self):
        doc_ids = list(self.snapshot.documents.keys())
        print(f'replacing tag values from {len(doc_ids)} document json files')
        document_json_paths = [self.snapshot_path.joinpath(doc_id).joinpath('document.json') for doc_id in doc_ids]
        with multiprocessing.Pool(processes=self.processes) as p:
            p.map(self._replace_tag_value_from_json, document_json_paths)


class Preprocessor:
    def __init__(self, snapshot: Snapshot, remove_old_data, processes):
        self.snapshot = snapshot
        self.processes = processes
        self.save_folder_path = Path(table_classifier.__file__).parent.parent.joinpath('data/raw_data')
        if remove_old_data:
            shutil.rmtree(self.save_folder_path, ignore_errors=True)

        self.class_folder_names = ['BS', 'IS', 'CF', 'Other']
        self.create_class_name_folders()

    def create_class_name_folders(self):
        for class_name in self.class_folder_names:
            os.makedirs(self.save_folder_path.joinpath(class_name), exist_ok=True)

    @staticmethod
    def get_table_class_label(tables: List[Field], table_class: Field) -> Union[TableTag, None]:
        try:
            table_class_tag = table_class.tags[0]
        except IndexError:
            return None

        table_class_tag_page_n = table_class_tag.page.page_number

        for table in tables:
            try:
                table_tag = table.tags[0]
            except IndexError:
                continue
            intersection_area = table_tag & table_class_tag
            area = table_class_tag.area
            if ((intersection_area / area) > 0.97) and (table_tag.page.page_number == table_class_tag_page_n):
                return table_tag
        return None

    def get_trs_column(self, df: pd.DataFrame) -> pd.Series:
        n_texts = []
        for col_n in df:
            n_text = self.get_n_text(col_n, df)
            n_texts.append(n_text)
        return df[np.argmax(n_texts)]

    @staticmethod
    def get_n_text(col_n, df):
        n_text = 0
        for value in df[col_n].to_list():
            parser = ValueParser(value)
            parser.parse()
            if parser.is_text():
                n_text += 1
        return n_text

    def get_table_label_pairs_from_document(self, doc_id: str):
        document = self.snapshot.documents[doc_id]
        if document is None:
            return
        for table_class in document.y['table_class']:
            table_tag = self.get_table_class_label(tables=document.x['table'], table_class=table_class)
            if table_tag is None:
                print(f'Could not find a matching table for {document.id}')
                return

            try:
                trs_column = self.get_trs_column(table_tag.df)
            except AttributeError:
                print(f'Could not get df for {doc_id}')
                return

            label = table_class.value if table_class.value != '' else 'Other'
            self.save_table(trs_column, label, doc_id, table_class.tags[0].page.page_number)

    def save_table(self, tr_column: pd.Series, label: str, doc_id: str, page_number: int):
        idx = random.randint(10000, 99999)
        save_path = self.save_folder_path.joinpath(label).joinpath(f'{doc_id}_{page_number}__{idx}.csv')
        tr_column.to_csv(path_or_buf=save_path, index=False, header=False)

    def get_table_label_pairs(self):
        doc_ids = list(self.snapshot.documents.keys())
        with multiprocessing.Pool(processes=self.processes) as p:
            p.map(self.get_table_label_pairs_from_document, doc_ids)


# Note - Do not run multiprocessing with console
if __name__ == '__main__':
    set_seeds()
    os.environ['SNAPSHOT_PATH'] = '/media/davidbudaghyan/a5bc3aee-bc65-41e0-87f7-0ca5c151697a/snapshots'
    os.environ['SNAPSHOT_ID'] = 'tables_classification_05_10'
    snapshot_ = Snapshot.get()
    snapshot_path_ = Path('/media/davidbudaghyan/a5bc3aee-bc65-41e0-87f7-0ca5c151697a/'
                          'snapshots/tables_classification_05_10')

    # cleaner = SnapshotCleaner(snapshot_, snapshot_path_, finished_doc_ids=finished_ids, processes=16)
    # cleaner.remove_if_not_finished_or_no_table_class_tagged()
    # cleaner.replace_tag_value_from_documents()

    preprocessor = Preprocessor(snapshot_, remove_old_data=True, processes=24)
    preprocessor.get_table_label_pairs()
