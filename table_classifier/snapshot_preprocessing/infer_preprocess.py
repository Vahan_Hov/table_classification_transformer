import os
import enum
import random
import shutil
import argparse
import numpy as np
import pandas as pd
from tqdm import tqdm
import multiprocessing
import table_classifier
from pathlib import Path
from typing import List, Union
from innolytiq import Snapshot
from innolytiq.document.field import Field
from innolytiq.document.tag import TableTag
from table_classifier.utils import set_seeds
from cdom.parsers.value_parser import ValueParser
import finspread.resources
from finspread.utils.matching.model_template import ModelTemplate


class TableTypesEnum(enum.Enum):
    bs_type = 'BS'
    is_type = 'IS'
    other_type = 'Other'


class InferPreprocessor:
    def __init__(self, snapshot: Snapshot, remove_old_data, processes, template_path, save_folder_path,
                 count_threshold=3, valid_table_account_count=5):
        self.snapshot = snapshot
        self.processes = processes
        self.save_folder_path = save_folder_path
        if remove_old_data:
            shutil.rmtree(self.save_folder_path, ignore_errors=True)

        self.create_class_name_folders()
        self.template_path = template_path
        self.model_template = ModelTemplate(template_path=str(self.template_path))
        self.bs_cognaize_ids = ('1050', '2200144', '3300144', '3301044', '3301044', '3400',
                                '3301444', '3300244', '3301144')
        self.is_cognaize_ids = ('6050', '6250', '6450', '3300855', '3300155', '3300355', '6850')
        self.count_threshold = count_threshold
        self.valid_table_account_count_threshold = valid_table_account_count

    def create_class_name_folders(self):
        for class_name in [TableTypesEnum.bs_type.value, TableTypesEnum.is_type.value, TableTypesEnum.other_type.value]:
            os.makedirs(self.save_folder_path.joinpath(class_name), exist_ok=True)

    @staticmethod
    def get_table_class_label(tables: List[Field], table_class: Field) -> Union[TableTag, None]:
        try:
            table_class_tag = table_class.tags[0]
        except IndexError:
            return None

        table_class_tag_page_n = table_class_tag.page.page_number

        for table in tables:
            try:
                table_tag = table.tags[0]
            except IndexError:
                continue
            intersection_area = table_tag & table_class_tag
            area = table_class_tag.area
            if ((intersection_area / area) > 0.97) and (table_tag.page.page_number == table_class_tag_page_n):
                return table_tag
        return None

    def get_trs_column(self, df: pd.DataFrame) -> pd.Series:
        n_texts = []
        for col_n in df:
            n_text = self.get_n_text(col_n, df)
            n_texts.append(n_text)
        return df[np.argmax(n_texts)]

    @staticmethod
    def get_n_text(col_n, df):
        n_text = 0
        for value in df[col_n].to_list():
            parser = ValueParser(value)
            parser.parse()
            if parser.is_text():
                n_text += 1
        return n_text

    def get_table_label_pairs_from_document(self, doc_id: str):
        document = self.snapshot.documents[doc_id]
        if document is None or 'table' not in document.x:
            return

        spreading_account_name_tags = self.get_account_name_tags(document)
        for table in document.x['table']:
            table_type = None
            try:
                table_accounts = self.get_table_accounts(spreading_account_name_tags, table)
            except IndexError:
                continue
            bs_count, is_count = self.get_bs_is_counts(table_accounts)
            if bs_count > self.count_threshold:
                table_type = TableTypesEnum.bs_type.value
            elif is_count > self.count_threshold:
                table_type = TableTypesEnum.is_type.value
            elif len(table_accounts) > self.valid_table_account_count_threshold:
                table_type = TableTypesEnum.other_type.value

            if table_type:
                self.save_table_trs_label(table, table_type, doc_id)

    def save_table_trs_label(self, table, table_type, doc_id):
        try:
            trs_column = self.get_trs_column(table.tags[0].df)
        except AttributeError:
            print(f'Could not get df for {doc_id}')
            return

        idx = random.randint(10000, 99999)
        save_path = f'{doc_id}_{table.tags[0].page.page_number}__{idx}.csv'
        save_path = self.save_folder_path.joinpath(table_type).joinpath(save_path)
        trs_column.to_csv(path_or_buf=save_path, index=False, header=False)

    def get_bs_is_counts(self, table_accounts):
        bs_count = 0
        is_count = 0
        for account_id in table_accounts:
            account_node = self.model_template.get_tree_node_by_account_field_id(account_id.split('__')[0])
            if account_node.cognaize_id in self.bs_cognaize_ids:
                bs_count += 1
            elif account_node.cognaize_id in self.is_cognaize_ids:
                is_count += 1
        return bs_count, is_count

    @staticmethod
    def get_table_accounts(spreading_account_name_tags, table):
        table_accounts = []
        for (name, tag) in spreading_account_name_tags:
            if tag.is_in_rectangle(table.tags[0], thresh=0.9) and \
                    tag.page.page_number == table.tags[0].page.page_number:
                table_accounts.append(name)
        return table_accounts

    @staticmethod
    def get_account_name_tags(document):
        spreading_account_name_tags = []
        for (name, account) in document.y.items():
            if name.endswith('__current') and account and account[0].tags and account[0].tags:
                spreading_account_name_tags.append((name, account[0].tags[0]))
        return spreading_account_name_tags

    def infer_save_table_types(self):
        doc_ids = list(sorted(self.snapshot.documents.keys()))
        pool = multiprocessing.Pool(processes=self.processes)
        # run multiprocessing pool and show progress bar
        for _ in tqdm(pool.imap_unordered(self.get_table_label_pairs_from_document, doc_ids), total=len(doc_ids)):
            pass

        # with multiprocessing.Pool(processes=self.processes) as p:
        #     p.map(self.get_table_label_pairs_from_document, doc_ids)


# Note - Do not run multiprocessing with console
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Spreading Evaluation')
    parser.add_argument('--SNAPSHOT_PATH', type=str, help='Path to the snapshot', default='')
    parser.add_argument('--SNAPSHOT_ID', type=str, help='SNAPSHOT ID', default='')
    parser.add_argument('--template_type', choices=['v1.4.1_mmas.xlsx', 'ESM_Template.xlsx'],
                        help='Template type', default='v1.4.1_mmas.xlsx')
    parser.add_argument('--output_path', type=str, help='Path to the directory results should be saved',
                        default=Path(table_classifier.__file__).parent.parent.joinpath('data/raw_data_inferred'))

    args = parser.parse_args()

    if not args.SNAPSHOT_PATH:
        print("Please define snapshot path. Use --SNAPSHOT_PATH.")
    elif not args.SNAPSHOT_ID:
        print("Please SNAPSHOT ID. Use --SNAPSHOT_ID.")
    else:
        set_seeds()
        os.environ["SNAPSHOT_PATH"] = args.SNAPSHOT_PATH
        os.environ["SNAPSHOT_ID"] = args.SNAPSHOT_ID
        snapshot_ = Snapshot.get()

        template_path_ = Path(finspread.resources.__file__).parent.joinpath(f'model-template/{args.template_type}')
        preprocessor = InferPreprocessor(snapshot_, template_path=template_path_, remove_old_data=False, processes=8,
                                         save_folder_path=args.output_path)
        preprocessor.infer_save_table_types()
