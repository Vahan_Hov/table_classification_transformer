import os
import shutil
from pathlib import Path
import numpy as np
import torch
import pandas as pd
from numpy.random import shuffle
from torch import Tensor
from functools import reduce

from torch.utils.data import WeightedRandomSampler

from table_classifier.utils import set_seeds, CFG
from typing import List, Iterable, Union
from torch.nn.utils.rnn import pad_sequence
from sentence_transformers import SentenceTransformer
from tqdm import tqdm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

device = 'cuda' if torch.cuda.is_available() else 'cpu'


class TrainValSplit:
    def __init__(self):
        self.bs_folder = CFG.unsplit_folder_path.joinpath(CFG.balance_sheet_label_name)
        self.is_folder = CFG.unsplit_folder_path.joinpath(CFG.income_statement_label_name)
        self.cs_folder = CFG.unsplit_folder_path.joinpath(CFG.cash_flow_label_name)

        self.bs_tables_paths = os.listdir(self.bs_folder)
        self.is_tables_paths = os.listdir(self.is_folder)
        self.cs_tables_paths = os.listdir(self.cs_folder)

        self._remove_folder(CFG.train_folder)
        self._remove_folder(CFG.val_folder)

        for folder in [CFG.train_folder, CFG.val_folder]:
            [os.makedirs(path, exist_ok=True) for path in [folder.joinpath(CFG.balance_sheet_label_name),
                                                           folder.joinpath(CFG.income_statement_label_name),
                                                           folder.joinpath(CFG.cash_flow_label_name)]]

    @staticmethod
    def _remove_folder(data_folder):
        print('removing existing data processed folder to create a new one')
        shutil.rmtree(data_folder, ignore_errors=True)

    @staticmethod
    def _copy_csvs(file_names: list, labels: list, source_folder: Path, destination_folder: Path):
        idx2save_path = {
            0: [source_folder.joinpath(CFG.balance_sheet_label_name),
                destination_folder.joinpath(CFG.balance_sheet_label_name)],
            1: [source_folder.joinpath(CFG.income_statement_label_name),
                destination_folder.joinpath(CFG.income_statement_label_name)],
            2: [source_folder.joinpath(CFG.cash_flow_label_name), destination_folder.joinpath(CFG.cash_flow_label_name)]
        }

        for file, label_idx in zip(file_names, labels):
            source_path = idx2save_path[label_idx][0].joinpath(file)
            destination_path = idx2save_path[label_idx][1].joinpath(file)
            shutil.copy(source_path, destination_path)

    def split_into_train_val(self):
        test_size = 0.15
        os.makedirs(CFG.train_folder, exist_ok=True)
        os.makedirs(CFG.val_folder, exist_ok=True)
        y = np.repeat(range(len(CFG.label_names)), [len(self.bs_tables_paths),
                                                    len(self.is_tables_paths),
                                                    len(self.cs_tables_paths)])

        X = self.bs_tables_paths + self.is_tables_paths + self.cs_tables_paths
        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=test_size, stratify=y)
        self._copy_csvs(X_train, y_train, source_folder=CFG.unsplit_folder_path, destination_folder=CFG.train_folder)
        self._copy_csvs(X_val, y_val, source_folder=CFG.unsplit_folder_path, destination_folder=CFG.val_folder)
        print(f'len train {len(X_train)}')
        print(f'len val {len(X_val)}')
        print(f'val size {len(X_val) / len(X_train)}')


class Preprocessor:
    def __init__(self, data_folder: Path = None, save_path: Path = None, embedding_model: SentenceTransformer = None,
                 batch_size=64, bert_batch_size=64, inference_mode=False):
        self.inference_mode = inference_mode
        self.save_folder_path = save_path

        self.le = LabelEncoder()
        self.le.fit(CFG.label_names)

        self.bs_folder = data_folder.joinpath(CFG.balance_sheet_label_name)
        self.is_folder = data_folder.joinpath(CFG.income_statement_label_name)
        self.cs_folder = data_folder.joinpath(CFG.cash_flow_label_name)

        self.bs_tables_paths = os.listdir(self.bs_folder)
        self.is_tables_paths = os.listdir(self.is_folder)
        self.cf_tables_paths = os.listdir(self.cs_folder)
        self.data = \
            [(self.bs_folder.joinpath(p), self.le.transform([CFG.balance_sheet_label_name])[0]) for p in
             self.bs_tables_paths] + \
            [(self.is_folder.joinpath(p), self.le.transform([CFG.income_statement_label_name])[0]) for p in
             self.is_tables_paths] + \
            [(self.cs_folder.joinpath(p), self.le.transform([CFG.cash_flow_label_name])[0]) for p in
             self.cf_tables_paths]

        self.batch_size = batch_size
        self.embedding_model = embedding_model
        self.bert_batch_size = bert_batch_size
        self.padding_value = -1

        if not inference_mode:
            self._remove_folder(self.save_folder_path)
            # TODO try batch sampler
            # class_counts = np.array([len(os.listdir(self.bs_folder)),
            #                          len(os.listdir(self.is_folder)),
            #                          len(os.listdir(self.others_folder))])
            #
            # num_samples = sum(class_counts)
            # # Note the order should be exactly like in CFG.label_names
            # labels = np.repeat(range(len(CFG.label_names)), [len(self.bs_tables_paths),
            #                                                  len(self.is_tables_paths),
            #                                                  len(self.others_tables_paths)])
            #
            # class_weights = [num_samples / class_counts[i] for i in range(len(class_counts))]
            # weights = [class_weights[labels[i]] for i in range(int(num_samples))]
            # self.batch_sampler = WeightedRandomSampler(torch.DoubleTensor(weights), int(num_samples))

    @staticmethod
    def _remove_folder(data_folder):
        print('removing existing data processed folder to create a new one')
        shutil.rmtree(data_folder, ignore_errors=True)

    def preprocess_save(self):
        n = len(self.data) // self.batch_size
        shuffle(self.data)
        if n == 0:
            n = 1
        splits = np.array_split(self.data, n)
        assert sum(len(f) for f in splits) == len(self.data)
        for i, file_names in tqdm(enumerate(splits)):
            batch = []
            for (file_name, label) in file_names:
                processed_file = self.preprocess_file(file_name, label)
                batch.append(processed_file)
            batch_processed = self._pad_batch(batch)
            path = os.path.join(self.save_folder_path, str(i) + '.pt')
            os.makedirs(self.save_folder_path, exist_ok=True)
            torch.save(batch_processed, path)

    def preprocess_file(self, file_path: str, label: int):
        df = pd.read_csv(filepath_or_buffer=file_path, dtype=str, header=None).fillna('')
        return self._preprocess_item(df, label)

    def preprocess_features(self, trs: pd.Series):
        item = self._preprocess_item(trs.fillna(''))
        return self._pad_batch([item])

    @staticmethod
    def _preprocess_item(trs: pd.Series, label: int = None):
        trs = trs[0].tolist()
        if label is not None:
            return trs, label
        else:
            return [trs]

    @staticmethod
    def _pad_sequences(sequences: Iterable) -> Tensor:
        lengths = []
        all_words = []
        for sub_sequence in sequences:
            lengths.append(len(sub_sequence))
            for words_embeddings in sub_sequence:
                words_tensor = torch.tensor(words_embeddings, dtype=torch.float32)
                all_words.append(words_tensor)

        padded_data = pad_sequence(all_words, batch_first=True)
        padded_data = torch.split(padded_data, lengths)
        return pad_sequence(padded_data, batch_first=True)

    def _pad_batch(self, batch) -> Union[List[Tensor], Tensor]:
        trs = [item[0] for item in batch]
        lengths_trs = [len(tr) for tr in trs]
        trs_flattened = reduce(lambda x, y: x + y, trs)
        trs_embedded = self.embedding_model.encode(trs_flattened, convert_to_tensor=True,
                                                   batch_size=self.bert_batch_size)
        trs_embedded = torch.split(trs_embedded, lengths_trs)
        trs_padded = pad_sequence(trs_embedded, batch_first=True)

        if not self.inference_mode:
            targets = torch.tensor([item[1] for item in batch])
            max_len = max(lengths_trs)
            padding_mask = []
            for length in lengths_trs:
                padding_mask.append(torch.cat([torch.zeros(length), torch.ones(max_len - length)], dim=0))
            padding_mask = torch.stack(padding_mask).bool()

            return [trs_padded, targets, padding_mask]
        return trs_padded


class PreprocessedDataset(torch.utils.data.Dataset):
    def __init__(self, data_folder):
        self.data_paths = [os.path.join(Path(data_folder).absolute(), p) for p in os.listdir(data_folder)]

    def __len__(self):
        return len(self.data_paths)

    def __getitem__(self, idx):
        batch = torch.load(self.data_paths[idx])
        return batch


def preprocess_data():
    set_seeds()
    if not CFG.train_folder.exists() or not CFG.val_folder.exists():
        TrainValSplit().split_into_train_val()

    embedding_model = SentenceTransformer(CFG.bert_supervised_model_name)
    print('Preprocessing train')
    preprocessor_train = Preprocessor(data_folder=CFG.train_folder, embedding_model=embedding_model,
                                      save_path=CFG.train_processed_folder, batch_size=32)
    preprocessor_train.preprocess_save()

    print('Preprocessing val')
    preprocessor_val = Preprocessor(data_folder=CFG.val_folder, embedding_model=embedding_model,
                                    save_path=CFG.val_processed_folder, batch_size=32)
    preprocessor_val.preprocess_save()


if __name__ == '__main__':
    preprocess_data()
