import os
import pathlib
import random
import torch
import numpy as np
import table_classifier
import pytorch_lightning as pl


class CFG:
    bert_supervised_model_name = 'distilbert-base-multilingual-cased'
    parent_dir = pathlib.Path(table_classifier.__file__).parent.parent.absolute()
    models_dir = parent_dir.joinpath('models')
    log_dir = parent_dir.joinpath('logs')

    account_classifier_model_path = parent_dir.joinpath('models/account_classifier_zions.pt')
    unsplit_folder_path = parent_dir.joinpath(f'data/raw_data')

    train_folder = parent_dir.joinpath(f'data/train/')
    train_processed_folder = parent_dir.joinpath(f'data/train_processed/')

    val_folder = parent_dir.joinpath(f'data/validation/')
    val_processed_folder = parent_dir.joinpath(f'data/validation_processed/')
    balance_sheet_label_name = 'BS'
    income_statement_label_name = 'IS'
    cash_flow_label_name = 'CF'
    label_names = [balance_sheet_label_name, income_statement_label_name, cash_flow_label_name]
    model_params = {
        'german_config': {
            "batch_size": 32,
            "lr": 0.00004,
            "nhead": 1,
            "nhid": 200,
            "nlayers": 1,
            "dropout": 0
        }
    }


def set_seeds(seed=42):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    pl.utilities.seed.seed_everything(seed=seed)
    torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
    torch.backends.cudnn.benchmark = False
