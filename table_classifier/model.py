import os
import math
from tqdm import tqdm
from typing import List

import pandas as pd
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers.tensorboard import TensorBoardLogger
from sentence_transformers import SentenceTransformer
from sklearn.metrics import f1_score, recall_score, precision_score

import torch
from torch import Tensor, nn
from torch.utils.data import DataLoader
from torch.nn import TransformerEncoderLayer, TransformerEncoder

from table_classifier.utils import set_seeds, CFG
from table_classifier.data_handling import PreprocessedDataset, Preprocessor

device = 'cuda' if torch.cuda.is_available() else 'cpu'


class PositionalEncoding(nn.Module):
    def __init__(self, d_model: int, max_len: int = 100, dropout: float = 0.1):
        super(PositionalEncoding, self).__init__()
        self.max_len = max_len
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(1).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x: Tensor) -> Tensor:
        x += self.pe[:, :x.shape[1], :]
        return self.dropout(x)


class TransformerModel(nn.Module):
    def __init__(self, out_shape_vocab_size: int, n_features: int,
                 max_len: int, nhead: int = 4, nlayers: int = 4, nhid: int = 200,
                 dropout: float = 0.5):
        super(TransformerModel, self).__init__()
        self.model_type = 'Transformer'
        encoder_layers = TransformerEncoderLayer(n_features, nhead, nhid, dropout, batch_first=True)
        self.pos_encoder = PositionalEncoding(d_model=n_features, max_len=max_len)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.ninp = n_features
        self.decoder = nn.Linear(n_features, out_shape_vocab_size)
        self.init_weights()

    def init_weights(self) -> None:
        initrange = 0.1
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    # src n_features must be divisible by 2
    def forward(self, src: Tensor, padding_mask: Tensor = None) -> Tensor:
        # TODO try with encoder
        # src = self.encoder(src) * math.sqrt(self.ninp)
        # src = self.pos_encoder(src)
        output = self.transformer_encoder(src, src_key_padding_mask=padding_mask)
        output = self.decoder(output)
        return output


class AccountClassifier(pl.LightningModule):
    def __init__(self, config: dict):
        super(AccountClassifier, self).__init__()
        dropout = config['dropout']
        nhead = config['nhead']
        nlayers = config['nlayers']
        nhid = config['nhid']
        self.batch_size = config['batch_size']
        self.lr = config['lr']
        self.transformer_output_size = 200

        self.embedding_model = SentenceTransformer(CFG.bert_supervised_model_name)

        self.preprocessor = Preprocessor(data_folder=CFG.train_folder, embedding_model=self.embedding_model,
                                         save_path=CFG.train_processed_folder, batch_size=config['batch_size'],
                                         inference_mode=True)

        self.classes = self.preprocessor.le.classes_
        self.n_classes = len(self.classes)

        self.n_features = self.embedding_model.get_sentence_embedding_dimension()

        self.criterion = nn.CrossEntropyLoss()

        self.transformer = TransformerModel(out_shape_vocab_size=self.transformer_output_size,
                                            n_features=self.n_features,
                                            max_len=200, nhead=nhead, nlayers=nlayers,
                                            nhid=nhid, dropout=dropout)

        self.classifier = nn.Sequential(
            nn.Linear(self.transformer_output_size, self.n_classes),
        )

    def forward(self, trs: Tensor, padding_mask: Tensor = None) -> Tensor:
        X = self.transformer(trs, padding_mask)

        if padding_mask is not None:
            mask_reversed = padding_mask.logical_not().unsqueeze(dim=2)
            X = (((mask_reversed * X).sum(dim=1)) / (mask_reversed.sum(dim=1)))
        else:
            X = torch.mean(X, dim=1)
        X = self.classifier(X)
        return X

    def training_step(self, batch: List[Tensor], batch_idx: int):
        return self._on_step(batch)

    def training_epoch_end(self, outputs: List[dict]):
        self._evaluate(outputs, mode='train')

    def validation_step(self, batch: List[Tensor], batch_idx: int):
        return self._on_step(batch)

    def validation_epoch_end(self, outputs: List[dict]):
        self._evaluate(outputs, mode='val')

    def _on_step(self, batch: List[Tensor]):
        trs, y, padding_mask = batch

        out = self(trs, padding_mask)

        loss = self.criterion(out, y.long())
        batch_dictionary = {
            "loss": loss,
            "y_pred": out.detach(),
            "y_true": y,
        }

        return batch_dictionary

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.lr)

    def _evaluate(self, outputs: List[dict], mode: str):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean().item()
        # TODO use torch accuracy metrics
        y_trues = [x['y_true'] for x in outputs]
        y_preds = [x['y_pred'] for x in outputs]

        y_preds = torch.cat(y_preds)
        y_preds = torch.argmax(y_preds, dim=1)
        y_trues = torch.cat(y_trues)

        y_preds = self.preprocessor.le.inverse_transform(y_preds.detach().cpu().numpy())
        y_trues = self.preprocessor.le.inverse_transform(y_trues.detach().cpu().numpy())

        f1 = f1_score(y_true=y_trues, y_pred=y_preds, average='weighted')
        recall = recall_score(y_true=y_trues, y_pred=y_preds, average='weighted')
        precision = precision_score(y_true=y_trues, y_pred=y_preds, average='weighted')

        self.log('step', self.current_epoch)
        self.log(f'{mode} loss', avg_loss, on_epoch=True, on_step=False)
        self.log(f'{mode} f1 score', f1, on_epoch=True, on_step=False)

        print(f'train epoch {self.current_epoch} {mode} loss', avg_loss)
        print(f'train epoch {self.current_epoch} {mode} precision', precision)
        print(f'train epoch {self.current_epoch} {mode} recall', recall)
        print(f'epoch {self.current_epoch} {mode} f1 score', f1)
        print('===============================\n')


def eval_all(model: nn.Module, epoch: int, mode: str):
    y_trues = []
    y_preds = []
    if mode == 'train':
        data_folder = CFG.train_folder
    elif mode == 'val':
        data_folder = CFG.val_folder
    else:
        raise Exception('Wrong mode given: expected either train or val')

    misclassified = []
    for class_folder_name in tqdm(os.listdir(data_folder), position=0):
        class_folder_path = os.path.join(data_folder, class_folder_name)
        for file in os.listdir(class_folder_path):
            csv_path = os.path.join(class_folder_path, file)
            # noinspection PyTypeChecker
            trs = pd.read_csv(csv_path, header=None)
            y_true = class_folder_name
            y_pred = infer(model, trs)
            y_trues.append(y_true)
            y_preds.append(y_pred)
            if y_true != y_pred:
                misclassified.append((file, y_true, y_pred))

    f1 = f1_score(y_true=y_trues, y_pred=y_preds, average='weighted')
    print(f'inference {epoch} {mode} f1 score {f1} num misclassified {len(misclassified)}')
    return misclassified


def load_model(model_path: str):
    state_dict = torch.load(model_path, map_location=device)['state_dict']
    model = AccountClassifier(config=CFG.model_params['german_config']).to(device)
    model.load_state_dict(state_dict)
    model.eval()
    return model


def infer(model: nn.Module, trs: list) -> str:
    trs = model.preprocessor.preprocess_features(trs)
    trs = trs.to(device)
    model = model.to(device)
    out = model(trs)
    y_pred = torch.argmax(out, 1).squeeze(0)
    return model.preprocessor.le.inverse_transform(([y_pred.item()]))[0]


def train(mode: str, config: dict, epochs: int):
    set_seeds()
    checkpoint_callback = ModelCheckpoint(
        monitor='val f1 score',
        dirpath=str(CFG.models_dir),
        filename=f'account_classifer_{mode}_bs--{config["batch_size"]}_nhid--{config["nhid"]}_lr--{config["lr"]}'
                 f'_n_layers--{config["nlayers"]}_n_head:{config["nhead"]}'
                 + '--{epoch:02d}--{val f1 score:.2f}',
        save_top_k=1,
        mode='max',
        # save_weights_only=True,
        every_n_epochs=1
    )
    train_dataset = PreprocessedDataset(data_folder=CFG.train_processed_folder)
    train_loader = DataLoader(train_dataset, batch_size=None,
                              shuffle=True,
                              drop_last=False)

    val_dataset = PreprocessedDataset(data_folder=CFG.val_processed_folder)
    val_loader = DataLoader(val_dataset, batch_size=None,
                            shuffle=False,
                            drop_last=False)

    model = AccountClassifier(config=config)
    logger_name = f'bs--{config["batch_size"]}_nhid--{config["nhid"]}_' \
                  f'lr--{config["lr"]}_n_layers--{config["nlayers"]}_n_head--{config["nhead"]}'
    logger = TensorBoardLogger(f"{CFG.log_dir}/tb_logs_{mode}",
                               name=logger_name, default_hp_metric=False)

    trainer = pl.Trainer(max_epochs=epochs,
                         logger=False,
                         # logger=logger,
                         gpus=-1,
                         # callbacks=checkpoint_callback,
                         callbacks=None,
                         deterministic=True,
                         )
    trainer.fit(model, train_dataloaders=train_loader, val_dataloaders=val_loader)
    return trainer, model


# TODO add header
# TODO bring code improvements from ctxlearn_transformer
if __name__ == '__main__':
    set_seeds()
    n_epochs = 100
    trainer, model_ = train('table_classification_test', config=CFG.model_params['german_config'], epochs=n_epochs)
    misclassified = eval_all(model=model_, epoch=n_epochs, mode='val')
