# Table Classifier Model

## Usage for inference

``` python
from table_classifer.model import load_model, infer

# Inputs are a list of all row Text Represenations and Account Types of a single table

trs = ['my', 'list', 'of', 'trs']

model = load_model(model_path='my/model/path')

predicted_table_type = infer(model, trs, account_types)
```
